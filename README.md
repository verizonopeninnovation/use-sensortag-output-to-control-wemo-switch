#use-SensorTag-output-to-control-wemo-switch#

This sample application uses output from the light sensor on the SensorTag to determine whether the Belkin WeMo Insight switch should be turned on or off.