package com.belkin.wemo;

import android.content.Context;
import android.util.Log;

import com.belkin.wemo.localsdk.WeMoDevice;
import com.belkin.wemo.localsdk.WeMoSDKContext;

import java.util.ArrayList;

/**
 * Created by piyush on 10/6/15.
 */
public class WemoInsight implements WeMoSDKContext.NotificationListener {

    private static Context context;
    private static WeMoSDKContext mWeMoSDKContext = null;
    private String TAG = this.getClass().getSimpleName();
    private static ArrayList<String> udns = null;
    private static ArrayList<WeMoDevice> wemoDevices = null;

    public WemoInsight(Context mContext) {

        Log.d(TAG, "Constructor called");
        mWeMoSDKContext = new WeMoSDKContext(mContext);
        mWeMoSDKContext.addNotificationListener(this);
        mWeMoSDKContext.refreshListOfWeMoDevicesOnLAN();
    }

    public void setState(int on_off) {
        Log.d(TAG, "setState called with value : " + String.valueOf(on_off));
        Log.d(TAG, "udns.size() : " + String.valueOf(udns.size()));

        for (int i = 0; i < udns.size(); i++) {
            WeMoDevice listDevice = mWeMoSDKContext.getWeMoDeviceByUDN(udns.get(i));

            Log.d(TAG, "listDevice availability is : " + listDevice.isAvailable());
            Log.d(TAG, "listDevice UDN is : " + listDevice.getUDN());
            Log.d(TAG, "listDevice type is : " + listDevice.getType());

            if (listDevice != null && listDevice.isAvailable()) {
                wemoDevices.add(listDevice);
                Log.d(TAG, "Serial number of the current device is : " + listDevice.getSerialNumber());
                Log.d(TAG, "current state of the device is : " + listDevice.getState().split("\\|")[0]);
                mWeMoSDKContext.setDeviceState(String.valueOf(on_off), udns.get(i));
            }
        }

    }

    @Override
    public void onNotify(final String event, final String udn) {
        Log.d(TAG, "onNotify method called with event: " + event);
        WeMoDevice wemoDevice = mWeMoSDKContext.getWeMoDeviceByUDN(udn);

            udns = mWeMoSDKContext.getListOfWeMoDevicesOnLAN();
            wemoDevices = new ArrayList<WeMoDevice>();
        Log.d(TAG, "onNotify udns.size() : " + String.valueOf(udns.size()));
    }
}